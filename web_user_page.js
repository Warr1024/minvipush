'use strict';

const path = require('path');
const fsp = require('fs')
	.promises;
const express = require('express');
const pug = require('pug');
const config = require('./config');

module.exports = async app => {
	app.use('/qrcode.js', express.static(path.join(__dirname,
		'node_modules', 'qrcode', 'build', 'qrcode.js')));
	for(const e of await fsp.readdir(path.join(__dirname, 'static')))
		app.use('/' + e, express.static(path.join(__dirname, 'static', e)));
	app.get('/manifest.json', (req, res) => (name => res.json({
		name: name,
		short_name: name,
		start_url: './',
		display: 'standalone',
		background_color: '#222',
		description: name,
		icons: [{
			purpose: 'maskable',
			src: 'icon-maskable.png',
			sizes: '256x256',
			type: 'image/png'
		}, {
			purpose: 'any',
			src: 'icon-matte.png',
			sizes: '192x192',
			type: 'image/png'
		}]
	}))(`${req.user.name} ${config.webname}`));
	if(config.customfiles)
		app.use('/custom', express.static(config.customfiles));

	const pugserve = (uri, file) => {
		const pagepug = pug.compileFile(path.join(__dirname, file));
		app.get(uri, async (req, res) => res
			.type('text/html')
			.send(pagepug({ config, user: req.user })));
	};
	pugserve('/', 'web_user_page_main.pug');
	pugserve('/view', 'web_user_page_view.pug');
};
