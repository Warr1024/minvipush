'use strict';

const bodyparser = require('body-parser');
const sqldb = require('./sqldb');
const log = require('./log');
const errguard = require('./errguard');
const sendworker = require('./sendworker');
const config = require('./config');
const pkid = require('./pkid');

module.exports = async app => {
	const ware = usebody => errguard(async (req, res) => {
		const data = Object.assign({},
			req.query,
			usebody ? { body: `${req.body}` } : {},
			req.user
		);
		const now = new Date()
			.getTime();
		await sqldb.queue(async trx => trx('sender')
			.insert({
				id: pkid(),
				stamp: Math.max(now - config.stampmaxpre,
					Math.min(now + config.stampmaxpost,
						data.stamp || now)),
				data: JSON.stringify(data)
			}));
		sendworker.sender();
		log({ sender: req.user, data });
		res.status(201);
		res.contentType('text/plain');
		res.end();
	});
	app.get('/text', ware(false));
	app.post('/text', bodyparser.text({ type: '*/*', }), ware(true));
};
