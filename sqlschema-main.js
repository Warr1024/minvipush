module.exports = {
	tables: {
		vapidkey: {
			columns: {
				pub: 'text not null',
				priv: 'text not null',
			},
		},
		sender: {
			columns: {
				key: 'text not null primary key',
				tag: 'boolean',
			},
			msgprops: true,
		},
		user: {
			columns: {
				key: 'text not null primary key',
				name: 'text'
			},
		},
		device: {
			columns: {
				userkey: 'text not null',
				hashkey: 'text not null',
				name: 'text',
				added: 'integer not null',
				pushdata: 'text not null',

			},
			primary: ['userkey', 'hashkey'],
			index: [
				['userkey']
			],
		},
		subscription: {
			columns: {
				userkey: 'text not null',
				senderkey: 'text not null',
			},
			msgprops: true,
			primary: ['userkey', 'senderkey'],
			index: [
				['userkey'],
				['senderkey'],
			]
		},
	},
	raw: [
		`CREATE TRIGGER vapidkey_singleton
		BEFORE INSERT ON vapidkey
		WHEN (SELECT COUNT(*) FROM vapidkey) >= 1
		BEGIN
			SELECT RAISE(FAIL, 'vapidkey singleton');
		END;`
	],
};
