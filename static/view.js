'use strict';
/* global window, document */

const el = n => document.getElementById(n);
const vis = (e, v) => e.classList[v ? 'remove' : 'add']('hide');
const msg = el('msg');

el('close')
	.addEventListener('click', () => window.close());

const linkify = t => {
	t = `${t}`;
	const div = document.createElement('div');
	const addel = tag => div.appendChild(document.createElement(tag));
	const addspan = s => addel('span')
		.innerText = s;
	let old = '';
	while(old !== t) {
		old = t;
		t = t.replace(/^(.*?)(https?:\/\/[^\s]+[^\s:\?\!\.])/is, (_, pre, uri) => {
			console.log({ pre, uri });
			addspan(pre);
			const link = addel('a');
			link.innerText = uri;
			link.setAttribute('href', uri);
			link.setAttribute('target', '_blank');
			return '';
		});
		console.log({ old, t });
	}
	if(t)
		addspan(t);
	console.log({ html: div.innerHTML });
	return div.innerHTML;
};

try {
	const data = JSON.parse(decodeURIComponent(window.location.hash.replace(/^#+/, '')));
	const bindprop = (name, withel) => {
		const ex = el(`${name}_ex`);
		if(!data[name])
			return vis(ex, false);
		const e = el(`${name}_in`);
		withel(e, data[name]);
		vis(ex, true);
	};
	bindprop('icon', (x, v) => x.setAttribute('src', v));
	bindprop('rawtitle', (x, v) => x.innerHTML = linkify(v));
	bindprop('body', (x, v) => x.innerHTML = linkify(v));
	bindprop('url', (x, v) => {
		v = new URL(v, `${window.location}`
			.replace(/#.*/, '')
			.replace(/view\/*/, ''));
		x.setAttribute('href', v);
		x.innerText = v;
	});
	bindprop('image', (x, v) => x.setAttribute('src', v));
	bindprop('stamp', (x, v) => {
		const dt = new Date(v);
		const offs = dt.getTimezoneOffset() * 60000;
		x.innerText = [
			new Date(v - offs)
			.toISOString()
			.replace('T', ' ')
			.replace('Z', ''),
			' UTC',
			(offs > 0 ? '-' : '+'),
			new Date(Math.abs(offs))
			.toISOString()
			.substring(11, 16)
		].join('');
	});
	const toptitle = el('toptitle');
	toptitle.innerText = `${data.rawtitle || data.title} - ${toptitle.innerText}`;
	vis(msg, false);
} catch (err) {
	msg.innerText = `${err}`;
	vis(msg, true);
}
