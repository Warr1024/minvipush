'use strict';

/* global window, document, navigator, fetch, Notification */

const el = n => document.getElementById(n);
const msg = el('msg');
const title = el('title');
const body = el('body');
const form = el('subscribe');
const namefield = el('devicename');
const submit = el('submit');

const hook = (obj, ...args) => obj.addEventListener ?
	obj.addEventListener(...args) :
	obj.attachEvent(...args);

const vis = (e, v) => e.classList[v ? 'remove' : 'add']('hide');

const showmsg = (t, b) => {
	title.innerText = t && `${t}` || '';
	body.innerText = b && `${b}` || '';
	vis(msg, t || b);
};

const offline = 'offline';

async function getsub() {
	try {
		const resp = await fetch('./online', { method: 'post' });
		if(resp.status !== 204) throw resp;
	} catch (err) {
		throw offline;
	}
	await Notification.requestPermission();
	if(!window.vapidKey)
		throw 'public key not installed';
	if(!navigator.serviceWorker)
		throw 'serviceWorker not supported';
	navigator.serviceWorker.register('svcworker.js', {
		updateViaCache: 'all'
	});
	const reg = await navigator.serviceWorker.ready;
	if(!reg.pushManager)
		throw 'pushManager not supported';
	return {
		mgr: reg.pushManager,
		sub: (await reg.pushManager.getSubscription())
	};
}

const checksub = () => getsub()
	.then(async ({ sub }) => {
		const subres = sub && await fetch('./subget', {
			method: 'post',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({ sub })
		});
		if(subres && subres.status === 200) {
			const data = await subres.json();
			showmsg('Subscribed', data.name);
			vis(form, false);
		} else {
			showmsg();
			vis(form, true);
		}
	})
	.catch(err => showmsg((err === offline) ? '' : 'Not Supported', err));

async function newsub(name) {
	let { sub, mgr } = await getsub();
	if(sub)
		await sub.unsubscribe();
	sub = await mgr.subscribe({
		userVisibleOnly: true,
		applicationServerKey: window.vapidKey
	});
	if(!sub)
		throw 'subscription failed';
	const subres = await fetch('./subadd', {
		method: 'post',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({ name, sub })
	});
	if(subres.status !== 204)
		throw await subres.text();
}

for(const x of ['change', 'keypress', 'input', 'cut', 'paste'])
	hook(namefield, x, () => setTimeout(() => vis(submit, namefield.value.trim()), 10));

hook(form, 'submit', async evt => {
	if(evt && evt.preventDefault) evt.preventDefault();
	const name = namefield.value.trim();
	if(!name) return;
	try {
		if(window.localStorage)
			window.localStorage.setItem('name', name);
		await newsub(name);
		checksub();
	} catch (err) {
		vis(form, false);
		showmsg('Error', err);
		setTimeout(checksub, 4000);
	}
	return false;
});

hook(window, 'beforeinstallprompt', async evt => { try { await evt.prompt(); } catch (e) {} });

(async () => {
	const canv = el('canvas');
	const expire = new Date()
		.getTime() * 10000;
	while(new Date()
		.getTime() < expire) {
		if(window.QRCode)
			return window.QRCode.toCanvas(canv, `${window.location}`, () => {
				canv.removeAttribute('style');
			});
		await new Promise(r => setTimeout(r, 10));
	}
})();

showmsg();
checksub();

if(window.localStorage)
	namefield.value = window.localStorage.getItem('name') || '';
vis(submit, namefield.value.trim());
