'use strict';

/* global self, Request, Response */

const cacheNamePage = `${new URL('page', self.location)}`;
const cacheNameStamp = `${new URL('stamp', self.location)}`;

const log = (msg, x) => (console.log(`${new Date().toISOString().substring(11)} ${self.location}: ${msg}`), x);

const aliveEvent = (name, cb) => self.addEventListener(name, event => {
	log(`event ${name}`);
	const p = cb(event);
	if (p && event.waitUntil)
		return event.waitUntil(p);
	return p;
});

aliveEvent('activate', () => self.clients.claim());

const fetchCached = async req => {
	const mylog = (msg, x) => log(`fetch ${req.url} -> ${msg}`, x);

	const cache = await self.caches.open(cacheNamePage);

	const cacheReq = new Request(req.url.replace(/#.*/, ''));
	const found = await cache.match(cacheReq);

	let modreq = req;
	if (found) {
		const etag = found.headers.get('ETag');
		if (etag) {
			const headers = {};
			if (etag) headers['If-None-Match'] = etag;
			modreq = new Request(req, {
				headers
			});
		}
	}

	const tasks = [];

	let timer;
	if (found)
		tasks.push(new Promise(res => timer = setTimeout(
			() => res(mylog('timeout', found)), 2000)));

	tasks.push((async () => {
		let resp;
		try {
			resp = await self.fetch(modreq);
		} catch (err) {
			if (found) return mylog(err, found);
			throw err;
		}
		if (!resp || !resp.ok || resp.status === 304)
			return mylog(resp ? resp.status : '!resp', found || resp);
		await cache.put(cacheReq, resp);
		return mylog(`fresh ${resp.status}`, await cache.match(cacheReq));
	})()
		.then(x => {
			if (timer) clearTimeout(timer);
			return x;
		}));

	return await Promise.race(tasks);
};

aliveEvent('install', async () => {
	const cache = await self.caches.open(cacheNamePage);
	const seen = [];
	const preload = async x => {
		if (seen[x]) return;
		seen[x] = true;
		try { log(`preload ${x}`, await cache.add(new Request(x))); }
		catch (err) { log(`preload ${x} ${err}`); }
	};
	await Promise.all([
		Promise.all([
			'./',
			'icon-clear.png',
			'icon-maskable.png',
			'icon-matte.png',
			'manifest.json',
			'pubkey.js',
			'page.css',
			'page.js',
			'qrcode.js',
			'view',
			'view.js',
			'view.css',
		].map(preload)),
		(async () => {
			const list = await (await fetchCached(new Request('preload'))).json();
			await Promise.all(list.map(preload))
		})()
	])
	self.skipWaiting();
});

const fetchToBase64 = async origurl => {
	try {
		const resp = await fetchCached(new Request(origurl));
		const blob = await resp.blob();
		const reader = new FileReader();
		await new Promise((resolve, reject) => {
			reader.onload = resolve;
			reader.onerror = reject;
			reader.readAsDataURL(blob);
		});
		return reader.result;
	} catch (err) {
		log(`${origurl} -> ${err}`);
		return origurl;
	}
};

aliveEvent('fetch', async event => {
	const req = event.request;
	const mylog = (msg, x) => log(`fetch ${req.url} -> ${msg}`, x);

	if (req.method === 'OPTIONS' || req.method === 'HEAD')
		return mylog(req.method);
	if (req.method !== 'GET')
		return mylog(req.method, event.respondWith(self.fetch(req)));

	return event.respondWith(fetchCached(req));
});

const stampCheck = async (title, opts) => {
	if (!opts.tag || !opts.stamp || opts.notag)
		return true;
	const cache = await self.caches.open(cacheNameStamp);
	const tagreq = new Request('/' + encodeURIComponent(opts.tag));
	const resp = await cache.match(tagreq);
	const old = resp && await resp.json();
	if (old > opts.stamp) {
		log(`message ${JSON.stringify(title)} stale ${opts.stamp} < ${old}`);
		return false;
	}
	await cache.put(tagreq, new Response(JSON.stringify(opts.stamp)));
	return true;
};

const notifyAllowProps = {
	actions: true,
	badge: true,
	body: true,
	data: true,
	dir: true,
	lang: true,
	icon: true,
	image: true,
	renotify: true,
	requireInteraction: true,
	silent: true,
	tag: true
};

const displayUrl = reluri => {
	try {
		const u = new URL(reluri, self.location);
		const v = new URL(self.location);
		const pref = [
			u.origin,
			`${u.protocol}//${u.hostname}`,
			u.protocol,
			''
		].find(x => v.href.substring(x.length) && v.href.substring(0, x.length) === x);
		return u.href.substring(pref.length);
	} catch (err) { }
	return reluri;
};

let pushRunning;
aliveEvent('push', async event => {
	while (pushRunning) await pushRunning;
	await (pushRunning = (async () => {
		try {
			const opts = event.data && JSON.parse(event.data.text()) || {};

			opts.body = opts.body || 'no text';

			const rawtitle = opts.title || 'no title';
			let title = rawtitle;
			if (opts.stamp)
				title += ' | ' + new Date(opts.stamp - new Date(opts.stamp)
					.getTimezoneOffset() * 60000)
					.toISOString()
					.substring(5, 19)
					.replace('T', ' ');
			if(!opts.silent)
				title += ' \ud83d\udd14';

			if (!opts.tag) {
				opts.notag = true;
				opts.tag = `${new Date().getTime() + Math.random()}`;
			}
			if (!stampCheck(title, opts))
				return;

			opts.actions = [];
			if (opts.url)
				opts.actions.push({ action: 'url', title: displayUrl(opts.url) });

			opts.data = Object.assign({}, opts, { title, rawtitle });
			delete opts.data.data;

			await Promise.all([
				(async () => {
					if (opts.badge)
						opts.badge = await fetchToBase64(opts.badge);
				})(),
				(async () => {
					if (opts.icon)
						opts.icon = await fetchToBase64(opts.icon);
				})(),
			]);

			for (const k in opts)
				if (opts.hasOwnProperty(k) && !notifyAllowProps[k])
					delete opts[k];

			log(`message ${JSON.stringify(title)}`);
			await self.registration.showNotification(title, opts);
		} finally { pushRunning = undefined; }
	})());
});

const geturl = (action, notification) => {
	if (action === 'image')
		return notification.data.image;
	if (action === 'url' && notification.data && notification.data.url)
		return notification.data.url;
	return new URL(`view#${encodeURIComponent(JSON.stringify(notification.data))}`,
		self.location);
};

aliveEvent('notificationclick', async event => {
	if (!event.notification.data) return;
	const openurl = geturl(event.action, event.notification);
	log(`notify click: ${openurl}`);
	try {
		if (openurl)
			await self.clients.openWindow(openurl);
	} catch (err) {
		log(`openWindow: ${err}`);
	} finally {
		const opts = event.notification.data;
		if (!stampCheck(opts.title, opts))
			return;
		await self.registration.showNotification(opts.title, Object.assign({},
			opts, {
			data: opts,
			silent: true
		}));
	}
});

aliveEvent('notificationclose', async event => {
	log(`notify close: ${event.notification.tag}`);
	if (!event.notification.tag) return;
	const cache = await self.caches.open(cacheNameStamp);
	const tagreq = new Request('/' + encodeURIComponent(event.notification.tag));
	await cache.delete(tagreq);
});

aliveEvent('pushsubscriptionchange', async event => {
	const sub = await self.registration.pushManager.subscribe(
		event.oldSubscription.options);
	await self.fetch('./subrenew', {
		method: 'post',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({ sub })
	});
});
