'use strict';

process.on('unhandledRejection', e => { throw e; });

const express = require('express');
const config = require('./config');
const log = require('./log');

const loadmods = async (app, ...list) => await Promise.all(list.map(async mod =>
	await require(`./web_${mod}`)(app)));

require('./sendworker');

(async () => {
	const inner = express();
	inner.use((req, res, next) => {
		res.setHeader('Content-Security-Policy',
			`block-all-mixed-content;default-src 'self' data:;base-uri
			'none';frame-ancestors 'none';`.replace(/\s+/gs, ' '));
		return next();
	});
	await loadmods(inner, 'health', 'flush', 'user', 'sender');

	const outer = express();
	outer.use(config.webroot, inner);

	outer.listen(config.webport, config.webhost, () =>
		log({ listening: [config.webhost, config.webport] }));
})();
