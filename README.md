# minvipush

Minimum Viable Push Notifications

## Concept

I have a large number of services that generate push messages that I want delivered to me, potentially across multiple devices.  Generally these messages are meant to communicate some small time-sensitive piece of information, such as:

- Status summary update from monitoring systems, machine reboots, UPS power status.
- Cron job exceptions or background job results.
- Weather alerts.
- Live streams for services I don't feel like setting up an account and subscribing.

### Alternatives

Some alternatives I've considered, and reasons why they weren't good enough:

- SMS using a 3rd party gateway: usually these have cost associated with them.
- SMS using email bridges: major email providers have repeatedly added restrictions on the ability to send email using systems such as msmtp, and sending email directly without a 3rd party provider generally only works from cloud hosting, not home internet.  The availabilty of email bridges is too scattered across services.  Spam defense measures also make email practically unusable for automated purposes.
- Plain email: email sending limitations as above, plus either needing a secondary mailbox for receiving, or cluttering up a primary email box.
- 3rd party chat services and other platforms like Slack, Discord, or PushBullet: these are often commercial and have arbitrary limits for unpaid accounts, plus their APIs are constantly changing and pushes stop working when old APIs are removed.

The original rationale for leaning so heavily on SMS is that back when I first started developing these systems (circa 2005) I had a phone only capable of receiving SMS, and it would be several years before I got a phone capable of running apps or browsers.  I continued to favor SMS because earlier cellular networks treated SMS differently than internet traffic, using a theoretically more reliable transport, and I had some concerns about data costs.  These reasons mostly do not apply for me anymore today.

### Goals/Constraints

After experimenting with many of the systems above, I've gotten a better idea of what it is I'm actually looking for.

- Send alerts to multiple devices, including both desktop and mobile, with reasonbly consistent UX.
- Battery-efficient for mobile devices: no wake-lock, no always-connected services.
- As few points of failure as possible.
- Self-hosted for (relative) privacy.
- Reasonable admin access.  Very simple web UI, usable direct access to back-end databases.

### Non-Goals

Things I've avoided including, to make the project simpler and get it completed quicker.  I *may* reconsider some of these in the future, but only if the lack of something causes me real problems.

- Minimal device enrollment UI.  It just enrolls devices to a user account and that's all; no management/config features.  Users should contact an admin to customize alerts for now (it's mostly designed for the "admin is the only user" use case anyway).
- No message history on the server.  Log to stderr, pipe to syslog if needed.
- No iOS support; I don't have an iPhone, and Apple mandating lack of support for certain web standards will continue to prevent me from having one.
- Don't worry about reliability, ordering, or batching of messages; fire them off to the push provider and let the provider and client deal with it.

### Architecture

- This project uses the [Web Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) to push messages to enrolled [Service Workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API) to display [Web Notifications](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API).

- The worker is part of a minimal [Progressive Web App](https://web.dev/progressive-web-apps/) that can be installed; on Android this allows independent customization of notification properties that *can't* currently be customized by the web app itself, such as ringtones.

- Messages can be sent via a simple web API which is designed to be easily usable via [curl](https://curl.se/), [wget](https://www.gnu.org/software/wget/), or any other simple web client tool or library.

- Messages are routed to recipients associated with a particular sender via subscriptions that are administratively controlled in the database.  Notification properties can be optionally defined via the sender API calls, defaulted or overridden via configuration on a per-sender or per-subscription basis.

- Message queueing is as robust as possible, so senders that ignore error responses and never retry will have the best possible chance of having their message queued, and failed message sends are retried automatically.

## Deployment

- Run `npm ci` to install dependencies.
- Run `node .` to run the server.
  - See `config.js` for config defaults, and use command line options to override them.
  - New databases are created automatically on startup if missing.
- The service must be reachable by the internet (reverse proxy with TLS recommended) in order for users to be able to enroll devices.
  - It *may* be possible to send messages even without web hosting capability, but not recommended long term.

## Administration

### Web UI

TBD

### Command Line

"Emergency" access by SSH or other text terminal methods.

- Use `node newkey` to generate a new secure random key, which is the primary key for users and senders.
  - Note that these keys should be considered ***secret*** as they grant access to features like sending and receiving messages to anyone who has them.
- Use the `sqlite3` command line tool, or any other tool of your choice, to edit the database and add/modify/remove rows.
- See `sqlschema-main.sql` for info about the schema.
  - Add `user`s to create new recipients.
  - Add `sender`s to create new senders.  Different categories of messages can use different senders to allow their audiences and presentation to differ.
  - Add `subscription`s to enroll users to the messages sent by a sender.
  - `device`s should be managed by the users themselves (unless you need to rename/remove one).

## User/Device Enrollment

Open a browser *on the device to be enrolled* to `http(s)://host/(webroot)`**`/u/(key)`**, where (key) is the key of a user, to access the user enrollment page.

- If the user-agent you are using is capable and appears to support the necessary protocols, you can subscribe it as a device for the displayed user by giving the device a name and hitting the Subscribe button, and if successful, it will receive all messages sent to that user.
- You can share this URL via the QRCode on the page, or by any other appropriate means, to get it onto other devices to enroll.
- Installing the page as a Progressive Web App on Android will allow you to customize the notification sound for it (on other platforms, for now it seems to be just a glorified bookmark).

Client side enrollment is mostly tested in Chrome (Android, Linux, Mac).  Safari is missing necessary Web APIs.  FireFox may or may not support them (I run it in incognito-only mode so it doesn't work for me).

## Message Send API

**N.B.** These API may give you a *false success* response but then never send the message.  Messages are accepted for queueing very aggressively, and certain validation only happens after the message has already been accepted.  If messages are not being sent, you may need to examine the server logs to find out why.

### Text POST

```sh
echo your message here | curl -fsL --retry 900 --retry-delay 1 --retry-all-errors -X POST --data-binary '@-' http(s)://host/(webroot)/s/(key)/text
```

(key) here is the key of the sender.  You can add optional parameters to the URL with properties and values supported by the Web Notification API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API).  The body of the notification message will always be the POST request body; any "body" query parameter will be ignored.

### Text GET

```sh
curl -fsL --retry 900 --retry-delay 1 --retry-all-errors http(s)://host/(webroot)/s/(key)/text?body=your%20message%20here
```

An alternative to the Text POST API that uses a body from a query paramter.  This may be easier to use under certain circumstances.

## Message Flush API

This API ensures that all messages associated with this sender at the time it's called have been sent to the 3rd party push provider and cleared the local queue, i.e. no message queued *before* calling this API is stuck in the local queue.

This is useful e.g. if you want to reboot the machine that's hosting the minvipush instance itself, and be sure that a "rebooting now" message has been sent before the reboot, instead of being stuck in the queue and only being sent *after* the reboot finishes.

```sh
curl -fsL --retry 900 --retry-delay 1 --retry-all-errors http(s)://host/(webroot)/s/(key)/flush
```
