'use strict';

module.exports = () => Math.sqrt(-2 * Math.log(Math.random())) *
	Math.cos(2 * Math.PI * Math.random());