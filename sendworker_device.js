'use strict';

const webpush = require('web-push');
const sqldb = require('./sqldb');
const errdump = require('./errdump');
const log = require('./log');
const config = require('./config');
const boxmuller = require('./boxmuller');
const pkid = require('./pkid');

const tryparse = t => {
	try { return JSON.parse(t); } catch (err) { errdump(err); }
};

const deleteDevice = async (sendworker, keys) => {
	const device = await sqldb.main(async trx => {
		const raw = await trx('device')
			.where(keys);
		const deviceGone = raw && raw[0] || keys;
		log({ deviceGone });
		await trx('device')
			.where(keys)
			.delete();
		return deviceGone;
	});
	await sqldb.queue(async trx => trx('user')
		.insert({
			id: pkid(),
			stamp: new Date()
				.getTime(),
			data: JSON.stringify({
				key: keys.userkey,
				title: config.webname,
				url: './',
				requireInteraction: true,
				body: `device gone: ${device.name || '?'}`
			})
		}));
	sendworker.user();
};

const msgTrunc = (devmsg, len) => {
	const body = devmsg.msg.body;
	const sendmsg = Object.assign({}, devmsg.msg);
	if (len !== undefined)
		sendmsg.body = `${body.substring(0, len)
			} [SNIP ${body.length - len}]`;
	const payload = JSON.stringify(sendmsg);
	const fits = webpush.encrypt(
		devmsg.push.keys.p256dh,
		devmsg.push.keys.auth,
		payload,
		'aes128gcm'
	)
		.cipherText
		.length <= config.maxpushsize;
	return { payload, fits };
};

const msgTryFit = devmsg => {
	let fullmsg = msgTrunc(devmsg);
	if (fullmsg.fits)
		return fullmsg;
	let min = 0;
	let max = devmsg.msg.body.length;
	while (min < max) {
		const trying = Math.ceil((min + max) / 2);
		fullmsg = msgTrunc(devmsg, trying);
		if (fullmsg.fits)
			min = trying;
		else
			max = trying - 1;
	}
	return msgTrunc(devmsg, min);
};

module.exports = async (sendworker, setnext) => {
	const devmsgs = (await sqldb.queue(async trx => trx('device')))
		.filter(x => {
			if (!x.next || x.next <= new Date()
				.getTime())
				return true;
			else
				setnext(x.next);
		})
		.map(x => Object.assign(tryparse(x.data) || {}, { id: x.id }));
	if (!devmsgs.length)
		return;
	await Promise.all(devmsgs
		.map(async devmsg => {
			const started = new Date()
				.getTime();
			const next = Math.max(config.retrymin * 2,
				Math.min(config.retrymax * 2,
					(started - devmsg.stamp)
				)
			) * (0.5 + boxmuller() * 0.1);
			await sqldb.queue(async trx => trx('device')
				.where({ id: devmsg.id })
				.update({ next }));
			let fullmsg = msgTryFit(devmsg);
			if (!fullmsg.fits) {
				log({ msgTooLong: devmsg });
				await sqldb.queue(async trx => await trx('device')
					.delete({ id: devmsg.id }));
				return;
			}
			const res = await (async () => {
				try {
					return await webpush.sendNotification(
						devmsg.push,
						fullmsg.payload);
				} catch (err) {
					if (err.statusCode)
						return err;
					throw err;
				}
			})();
			const statusType = Math.floor(res.statusCode / 100);
			log({ devmsg, res, statusType });
			if (statusType === 2 || (statusType === 4 && res.statusCode !== 429))
				await sqldb.queue(async trx => await trx('device')
					.delete({ id: devmsg.id }));
			else
				setnext(next);
			if (res.statusCode === 410)
				await deleteDevice(sendworker, {
					userkey: devmsg.userkey,
					hashkey: devmsg.hashkey
				});
		}));
	sendworker.flush();
};
