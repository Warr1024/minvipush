'use strict';

const config = require('./config');
const sqldb = require('./sqldb');
const errguard = require('./errguard');

module.exports = async app => {
	app.get('/health', errguard(async (req, res) => {
		const expire = new Date().getTime() - config.stalltime;
		const stalled = await Promise.all(['sender', 'user', 'device']
			.map(async tbl => await sqldb.queue(async trx => await trx(tbl)
				.where('stamp', '<', expire)
				.select('stamp'))));
		if (stalled.find(x => x.length))
			throw Error('queue stalls');
		res.sendStatus(204);
	}));
};

