'use strict';

const os = require('os');
const path = require('path');
const minimist = require('minimist');

module.exports = minimist(process.argv.slice(2), {
	default: {
		webport: 8080,
		webhost: '0.0.0.0',
		webroot: '/',
		webname: `${os.hostname().replace(/\..*/, '')} minvipush`,
		datapath: path.join(__dirname, 'data'),
		vapidcontact: 'mailto:root@example.org',
		maxpushsize: 4096,
		customfiles: '',
		notifytitle: `${os.hostname().replace(/\..*/, '')} minvipush`,
		notifyicon: 'icon-clear.png',
		notifybadge: 'icon-clear.png',
		workerinterval: 600000,
		retrymin: 1000,
		retrymax: 600000,
		stampmaxpre: 3600000,
		stampmaxpost: 0,
		stalltime: 60000,
		flushtimeout: 60000,
	}
});
