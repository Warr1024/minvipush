'use strict';

const errdump = require('./errdump');

module.exports = func => async (req, res, ...more) => {
	try {
		return await func(req, res, ...more);
	} catch (err) {
		const errid = errdump(err);
		res.status(500);
		res.json({ errid });
	}
};
