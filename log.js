'use strict';

const rxesc = s => s.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
const stackrx = new RegExp(`.*${rxesc(__dirname)}/`);

module.exports = msg => console.log(JSON.stringify(Object.assign({
	time: new Date().toISOString(),
	at: new Error().stack.split('\n')[2]
		.trim()
		.replace(stackrx, '')
		.replace(/\)+$/, '')
},
	(typeof msg === 'object' && `${msg}` === '[object Object]') ? msg : { msg })));
