'use strict';

const msgprops = require('./msgprops');

const catcher = err => {
	if (!err.message) throw err;
	if (err.message.indexOf('SQLITE_ERROR: duplicate column name') >= 0) return;
	if (/SQLITE_ERROR: trigger \S+ already exists/.test(err.message)) return;
	throw err;
};

const sqlschema = async (db, schema) => {
	for (const [tname, tdef] of Object.entries(schema.tables || {})) {
		if (tdef.msgprops) {
			for (const [k, v] of Object.entries(msgprops))
				tdef.columns[k] = v;
			delete tdef.msgprops;
		}
		await db.schema.createTable(tname, t => {
			for (const [cname, ctype] of Object.entries(tdef.columns))
				t.specificType(cname, ctype);
		}).catch(async () => {
			for (const [cname, ctype] of Object.entries(tdef.columns))
				db.schema.alterTable(tname, t => t.specificType(cname, ctype))
					.catch(catcher);
		}).catch(catcher);

		if (tdef.primary)
			db.schema.alterTable(tname, t => t.primary(tdef.primary))
				.catch(catcher);
		for (const cols of (tdef.index || []))
			db.schema.alterTable(tname, t => t.index(cols));
	}
	for (const raw of (schema.raw || []))
		await db.raw(raw.replace(/\s*\n\s*/g, ' ')).catch(catcher);
};

module.exports = sqlschema;
