module.exports = {
	tables: {
		sender: {
			columns: {
				id: 'integer not null primary key',
				stamp: 'integer not null',
				data: 'text not null',
			},
		},
		user: {
			columns: {
				id: 'integer not null primary key',
				stamp: 'integer not null',
				data: 'text not null',
			},
		},
		device: {
			columns: {
				id: 'integer not null primary key',
				stamp: 'integer not null',
				next: 'integer',
				data: 'text not null',
			},
		},
	},
};
