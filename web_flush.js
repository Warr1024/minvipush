'use strict';

const config = require('./config');
const errguard = require('./errguard');
const flush = require('./sendworker_flush').flush;

module.exports = async app => {
	app.get('/flush', errguard(async (req, res) =>
		res.sendStatus(await Promise.race([
			flush().then(() => 204),
			new Promise(res => setTimeout(() => res(503), config.flushtimeout)),
		]))));
};

