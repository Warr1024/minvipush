'use strict';

const log = require('./log');

let lasterrid = 0;

module.exports = err => {
	let errid = new Date().getTime();
	if (errid <= lasterrid) errid = lasterrid + 1;
	lasterrid = errid;
	err.stringified = `${err}`;
	log({ errid, err });
	return errid;
};
