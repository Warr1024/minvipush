'use strict';

const sqldb = require('./sqldb');
const errdump = require('./errdump');
const log = require('./log');
const senddevice = require('./senddevice');
const msgprops = require('./msgprops');

const dedupe = arr => (seen => arr.filter(k => {
	const x = seen[k];
	seen[k] = true;
	return !x;
}))({});

const tryparse = t => {
	try { return JSON.parse(t); } catch (err) { errdump(err); }
};

const prefextract = (obj, pref) => Object.assign({}, ...Object.entries(obj)
	.filter(([k, v]) => k.startsWith(pref) && v !== null)
	.map(([k, v]) => ({ [k.substring(pref.length)]: v })));

module.exports = async sendworker => {
	const rawmsgs = await (async () => {
		const raw = await sqldb.queue(async trx => trx('sender'));
		return raw.map(x => Object.assign(tryparse(x.data) || {}, { id: x.id, stamp: x.stamp }));
	})();
	if (!rawmsgs.length)
		return;

	const subidx = await (async () => {
		const allkeys = dedupe(rawmsgs.map(x => x.key))
			.sort();
		const subraw = await sqldb.main(async trx => trx.raw(`
			select
				s.key,
				d.userkey,
				d.hashkey,
				${Object.keys(msgprops).map(k => `s.${k} as s_${k}, `).sort().join('')}
				s.tag,
				${Object.keys(msgprops).map(k => `u.${k} as u_${k}, `).sort().join('')}
				d.pushdata
			from sender s
			inner join subscription u
				on u.senderkey = s.key
			inner join device d
				on d.userkey = u.userkey
			where s.key in (${allkeys.map(() => '?').join(',')})
		`, allkeys));
		const subidx = {};
		for (const x of subraw) {
			x.pushdata = tryparse(x.pushdata);
			(subidx[x.key] = subidx[x.key] || {})[x.hashkey] = x;
		}
		return subidx;
	})();

	let dirty;
	await Promise.all(rawmsgs.map(async rawmsg =>
		await sqldb.queue(async trx => {
			const recvs = subidx[rawmsg.key];
			if (recvs) {
				await Promise.all(Object.values(recvs)
					.map(async recv => senddevice(
						trx,
						rawmsg,
						recv,
						prefextract(recv, 's_'),
						rawmsg,
						prefextract(recv, 'u_')
					)));
				dirty = true;
			} else
				log({ noSubscriptions: rawmsg });
			await trx('sender').delete({ id: rawmsg.id });
		})));

	if (dirty)
		sendworker.device();
};
