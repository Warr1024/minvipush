'use strict';

const webpush = require('web-push');
const config = require('./config');
const sqldb = require('./sqldb');
const log = require('./log');

module.exports = (async () => {
	const getkeys = async () => {
		const keys = await sqldb.main(t => t('vapidkey').limit(1));
		if (!keys || !keys[0]) return;
		webpush.setVapidDetails(config.vapidcontact, keys[0].pub, keys[0].priv);
		return keys[0].pub;
	};

	const loaded = await getkeys();
	if (loaded) return (log({ vapid: 'loaded' }), loaded);

	const newkeys = webpush.generateVAPIDKeys();
	await sqldb.main(t => t('vapidkey').insert({
		pub: newkeys.publicKey,
		priv: newkeys.privateKey
	}));

	const created = await getkeys();
	if (created) return (log({ vapid: 'created' }), created);

	throw Error('key generation failed');
})();
