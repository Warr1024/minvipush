'use strict';

const bodyparser = require('body-parser');
const sqldb = require('./sqldb');
const log = require('./log');
const errguard = require('./errguard');
const sendworker = require('./sendworker');
const config = require('./config');
const myhash = require('./myhash');
const pkid = require('./pkid');

const subhash = sub => myhash([
	sub.endpoint,
	...Object.entries(sub.keys)
		.sort((a, b) => a[0].localeCompare(b[0]))
]);

const usermsg = async (req, body) => {
	await sqldb.queue(async trx => trx('user')
		.insert({
			id: pkid(),
			stamp: new Date()
				.getTime(),
			data: JSON.stringify({
				key: req.user.key,
				title: config.webname,
				url: './',
				body
			})
		}));
	sendworker.user();
};

module.exports = async app => {
	app.post('/online', (req, res) => res.sendStatus(204));
	app.post('/subget', bodyparser.json(), errguard(async (req, res) => {
		const hashkey = subhash(req.body.sub);
		const data = await sqldb.main(async trx => await trx('device')
			.where({ hashkey, userkey: req.user.key })
			.select('name'));
		if (!data || !data[0])
			return res.sendStatus(404);
		return res.json(data[0]);
	}));
	app.post('/subadd', bodyparser.json(), errguard(async (req, res) => {
		const hashkey = subhash(req.body.sub);
		await sqldb.main(async trx => await trx('device')
			.insert({
				userkey: req.user.key,
				hashkey,
				name: req.body.name,
				added: new Date()
					.getTime(),
				pushdata: JSON.stringify(req.body.sub)
			}));
		log({ subscription: 'added', user: req.user, body: req.body });
		await usermsg(req, `device added: ${req.body.name}`);
		res.sendStatus(204);
	}));
	app.post('/subrenew', bodyparser.json(), errguard(async (req, res) => {
		const keys = {
			userkey: req.user.key,
			hashkey: subhash(req.body.sub)
		};
		const device = await sqldb.main(async trx => {
			await trx('device')
				.where(keys)
				.update({
					pushdata: JSON.stringify(req.body.sub)
				});
			return (await trx('device')
				.where(keys))[0];
		});
		log({ subscription: 'renewed', user: req.user, body: req.body });
		await usermsg(req, `device renewed: ${device && device.name || keys.hashkey}`);
		res.sendStatus(204);
	}));
	app.get('/preload', errguard(async (req, res) => {
		const imgs = {};
		await Promise.all(['icon', 'badge'].map(async col => await sqldb.main(async trx => {
			for (const row of await trx.raw(`
				select distinct coalesce(sub.${col}, send.${col}) as img
				from subscription sub
				inner join sender send on send.key = sub.senderkey
				where sub.userkey = ?`,
				[req.user.key]))
				if (row && row.img)
					imgs[row.img] = true;
		})));
		return res.json(Object.keys(imgs).sort());
	}));
};
