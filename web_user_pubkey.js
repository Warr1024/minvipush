'use strict';

const loadingpubkey = require('./pubkey');

module.exports = async app => {
	const pubkey = await loadingpubkey;
	const padding = '='.repeat((4 - pubkey.length % 4) % 4);
	const b64pubkey = (pubkey + padding)
		.replace(/\-/g, '+')
		.replace(/_/g, '/');
	const pubkeyarray = Array.from(Buffer.from(b64pubkey, 'base64'));
	const pkjs = `window.vapidKey = Uint8Array.from(${JSON.stringify(pubkeyarray)});`;
	app.get('/pubkey.js', async (req, res) => res
		.type('application/javascript')
		.send(pkjs));
};
