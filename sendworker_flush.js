'use strict';

const sqldb = require('./sqldb');

let workers;
let awaiting;

module.exports = async sendworker => {
	workers = sendworker;
	if (!awaiting) return;
	if (!(await sqldb.queue(
		async trx => await Promise.all(
			['device', 'sender', 'user']
				.map(async tn => (await trx(tn).select('stamp').limit(1)).length)
		)
	)).find(x => x))
		awaiting.resolve();
};

module.exports.flush = async () => {
	if (!awaiting) {
		let resolve;
		const promise = new Promise(res => resolve = res);
		awaiting = { resolve, promise };
		if (workers)
			workers.flush();
	}
	await awaiting.promise;
};
