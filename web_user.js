'use strict';

const express = require('express');
const sqldb = require('./sqldb');
const errguard = require('./errguard');

module.exports = async outer => {
	const inner = await express();
	await Promise.all('page pubkey api'.split(' ')
		.map(async n => await require(`./web_user_${n}`)(inner)));
	outer.use('/u/:key$', (req, res) => res.redirect(req.params.key + '/'));
	outer.use('/u/:key/', errguard(async (req, res, next) => {
		req.user = (await sqldb.main(async trx => await trx('user')
			.where({ key: req.params.key })))[0];
		if (!req.user)
			return res.sendStatus(404);
		return await next();
	}), inner);
};
