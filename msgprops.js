'use strict';

module.exports = {
	badge: 'text',
	body: 'text',
	dir: 'text',
	lang: 'text',
	icon: 'text',
	image: 'text',
	renotify: 'boolean',
	requireInteraction: 'boolean',
	silent: 'boolean',
	title: 'text',
	url: 'text'
};
