'use strict';

const express = require('express');
const errguard = require('./errguard');

module.exports = async outer => {
	const inner = await express();
	await Promise.all('text'.split(' ')
		.map(async n => await require(`./web_sender_${n}`)(inner)));
	outer.use('/s/:key$', (req, res) => res.redirect(req.params.key + '/'));
	outer.use('/s/:key/', errguard(async (req, res, next) => {
		if (req.params && req.params.key)
			req.user = { key: `${req.params.key}` };
		if (!req.user)
			return res.sendStatus(404);
		return await next();
	}), inner);
};
