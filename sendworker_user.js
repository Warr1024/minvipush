'use strict';

const sqldb = require('./sqldb');
const errdump = require('./errdump');
const log = require('./log');
const senddevice = require('./senddevice');

const dedupe = arr => (seen => arr.filter(k => {
	const x = seen[k];
	seen[k] = true;
	return !x;
}))({});

const tryparse = t => {
	try { return JSON.parse(t); } catch (err) { errdump(err); }
};

module.exports = async sendworker => {
	const rawmsgs = await (async () => {
		const raw = await sqldb.queue(async trx => trx('user'));
		return raw.map(x => Object.assign(tryparse(x.data) || {}, { id: x.id, stamp: x.stamp }));
	})();
	if (!rawmsgs.length)
		return;

	const useridx = await (async () => {
		const allkeys = dedupe(rawmsgs.map(x => x.key))
			.sort();
		const subraw = await sqldb.main(async trx => trx('device')
			.whereIn('userkey', allkeys));
		const subidx = {};
		for (const x of subraw) {
			x.pushdata = tryparse(x.pushdata);
			(subidx[x.userkey] = subidx[x.userkey] || {})[x.hashkey] = x;
		}
		return subidx;
	})();

	let dirty;
	await Promise.all(rawmsgs.map(async rawmsg =>
		await sqldb.queue(async trx => {
			const recvs = useridx[rawmsg.key];
			if (recvs) {
				await Promise.all(Object.values(recvs)
					.map(async recv => senddevice(
						trx,
						rawmsg,
						recv,
						rawmsg
					)));
				dirty = true;
			} else
				log({ noSubscriptions: rawmsg });
			await trx('user')
				.delete({ id: rawmsg.id });
		})));

	if (dirty)
		sendworker.device();
};
