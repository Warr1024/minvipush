'use strict';

const config = require('./config');
const sqldb = require('./sqldb');
const myhash = require('./myhash');
const msgprops = require('./msgprops');
const pkid = require('./pkid');

const globalDefaults = (prefix =>
	Object.assign({},
		...Object.entries(config)
			.filter(([k]) => k.startsWith(prefix))
			.map(([k, v]) => ({
				[k.substring(prefix.length)]: v
			})))
)('notify');

const filters = {
	text: x => `${x}`,
	boolean: x => !!x,
	number: x => Number(x)
};
const myprops = Object.assign({}, ...[
	...Object.entries(msgprops),
	['stamp', 'number']
].map(([k, v]) => ({ [k]: filters[v] })));

const senddevice = async (trx, rawmsg, recv, ...layers) => {
	const data = Object.assign({},
		globalDefaults,
		...layers);

	const msg = {};
	for (const [key, func] of Object.entries(myprops)) {
		const val = data[key];
		if (val !== null && val !== undefined)
			msg[key] = func(val);
	}

	if (recv.tag) {
		let tagparts = [rawmsg.key];
		if (data.tag)
			tagparts.push(JSON.stringify(data.tag));
		msg.tag = myhash(tagparts);
		if (!msg.hasOwnProperty('renotify') && !msg.silent)
			msg.renotify = true;
	}

	const commit = async t => await t('device')
		.insert({
			id: pkid(),
			stamp: rawmsg.stamp,
			data: JSON.stringify({
				msg,
				userkey: recv.userkey,
				hashkey: recv.hashkey,
				push: recv.pushdata
			})
		});
	await (trx ? commit(trx) : sqldb.queue(commit));
};

module.exports = senddevice;
