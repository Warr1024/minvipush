'use strict';

const path = require('path');
const fsx = require('fs-extra');
const knex = require('knex');
const config = require('./config');
const log = require('./log');
const sqlschema = require('./sqlschema');

const alreadyinit = {};

const mkdb = name => async () => {
	await fsx.ensureDir(config.datapath);
	const filename = path.join(config.datapath, `${name}.sqlite`);
	const existed = await fsx.pathExists(filename);
	const db = await knex({
		client: 'sqlite3',
		useNullAsDefault: true,
		connection: { filename }
	});
	if (alreadyinit[name]) return db;
	log({ schema: 'install', name });
	if (!existed)
		await db.raw(`pragma journal_mode='wal'`);
	await sqlschema(db, require(`./sqlschema-${name}`));
	alreadyinit[name] = true;
	return db;
};

const main = (() => {
	const mdb = mkdb('main');
	const init = (async () => await (await mdb()).destroy())()
		.then(() => log({ dbready: 'main' }));
	return async func => {
		await init;
		const db = await mdb();
		try {
			return await db.transaction(func);
		}
		finally {
			db.destroy();
		}
	};
})();

const queue = (() => {
	const qdb = mkdb('queue')();
	qdb.then(() => log({ dbready: 'queue' }));
	return async func =>
		await (await qdb).transaction(func);
})();

module.exports = { main, queue };
