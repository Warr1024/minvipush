'use strict';

const boxmuller = require('./boxmuller');
const config = require('./config');
const errdump = require('./errdump');
const log = require('./log');

const sendworker = {
	sender: true,
	user: true,
	device: true,
	flush: true
};

const mkworker = k => {
	const func = require(`./sendworker_${k}`);
	let running;
	let requested;
	let pended;
	const request = () => {
		if(pended) clearTimeout(pended);
		requested = true;
		if(running) return;
		requested = undefined;
		running = (async () => {
			log({ sendworker: k });
			let next = new Date()
				.getTime() + config.workerinterval *
				(1 + 0.1 * boxmuller());
			const setnext = n => next = (n < next) ? n : next;
			try {
				await func(sendworker, setnext);
			} catch (err) {
				errdump(err);
			} finally {
				running = undefined;
				if(requested)
					setImmediate(request);
				else {
					const delay = next - new Date()
						.getTime();
					if(delay <= 0)
						setImmediate(request);
					else
						pended = setTimeout(request, delay);
				}
			}
		})();
	};
	setImmediate(request);
	return request;
};

for(const k of Object.keys(sendworker))
	sendworker[k] = mkworker(k);

module.exports = sendworker;
