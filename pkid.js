'use strict';

let nextid = new Date().getTime();

module.exports = () => nextid++;
